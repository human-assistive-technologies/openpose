# -*- coding: utf-8 -*-
"""
Created on Wed Mar 10 01:07:55 2021

@author: HAT_NUC1
"""

# First import the library
import cv2
import mediapipe as mp
import numpy as np
import time
from realsense_depth import *

mp_drawing = mp.solutions.drawing_utils
mp_pose = mp.solutions.pose

# Initialize Camera Intel Realsense
dc = DepthCamera()

print("Open pose starting\n")



def calculate_angle(a,b,c):
    a = np.array(a) # First
    b = np.array(b) # Mid
    c = np.array(c) # End
    
    radians = np.arctan2(c[1]-b[1], c[0]-b[0]) - np.arctan2(a[1]-b[1], a[0]-b[0])
    angle = np.abs(radians*180.0/np.pi)
    
    if angle >190.0:
        angle = 360-angle
        
    return angle


red = (0, 0, 250)
blue = (255, 0, 0)

color = blue;
pTime = 0


 ## open-pose and angle
cap = cv2.VideoCapture(1)
## Setup mediapipe instance
with mp_pose.Pose(min_detection_confidence=0.5, min_tracking_confidence=0.5) as pose:
    while cap.isOpened():
        
        #Start depth
        ret, depth_frame, color_frame = dc.get_frame()
        
        
                
        #cv2.imshow("depth frame", depth_frame)
                
        #Start openpose
        ret, frame = cap.read()
        
        # Recolor image to RGB
        image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        image.flags.writeable = False
      
        # Make detection
        results = pose.process(image)
    
        # Recolor back to BGR
        image.flags.writeable = True
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
        
        # Extract landmarks
        try:
            landmarks = results.pose_landmarks.landmark
            
            # Get coordinates
            shoulder = [landmarks[mp_pose.PoseLandmark.LEFT_SHOULDER.value].x,landmarks[mp_pose.PoseLandmark.LEFT_SHOULDER.value].y]
            elbow = [landmarks[mp_pose.PoseLandmark.LEFT_ELBOW.value].x,landmarks[mp_pose.PoseLandmark.LEFT_ELBOW.value].y]
            wrist = [landmarks[mp_pose.PoseLandmark.LEFT_WRIST.value].x,landmarks[mp_pose.PoseLandmark.LEFT_WRIST.value].y]
            
            # Calculate angle
            angle = calculate_angle(shoulder, elbow, wrist)
            
            if angle > 80 and angle < 100 : 
                cv2.putText(image, "Position - OK", (400, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (blue), 1, cv2.LINE_AA)  
                color=blue
            else:                
                cv2.putText(image, "Position - FAIL", (400, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (red), 1, cv2.LINE_AA)  
                color=red
            
            # Visualize angle
            offset = 20
            cv2.putText(image, str(angle), 
                           tuple(np.multiply(elbow, [640+offset, 480]).astype(int)), 
                           cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)  
            
            
            #Point 1
            point1 = tuple(np.multiply(elbow, [640, 480]).astype(int))
            #print("Point: ",point1)
            
            cv2.circle(image,point1,4,(0,0,255))
            distance = depth_frame[point1[1],point1[0]]            
            cv2.putText(image, "{}mm".format(distance), 
                         (point1[0]+offset, point1[1] + offset*2),
                         cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 0), 2)
            
            #Point 2
            point2 = tuple(np.multiply(shoulder, [640, 480]).astype(int))
            #print("Point 2: ",point2)
            
            cv2.circle(image,point2,4,(0,255,0))
            distance2 = depth_frame[point2[1],point2[0]] 
            cv2.putText(image, "{}mm".format(distance2), 
                         (point2[0]+offset, point2[1] + offset*2),
                         cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 0), 2) 
            
            #Point 3
            point3 = tuple(np.multiply(wrist, [640, 480]).astype(int))
            #print("Point 2: ",point2)
            
            cv2.circle(image,point3,4,(0,255,0))
            distance3 = depth_frame[point3[1],point3[0]] 
            cv2.putText(image, "{}mm".format(distance3), 
                         (point3[0]+offset, point3[1] + offset*2),
                         cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 0), 2) 
            
            
            
        except:
            pass
        
        
        
        # Render detections    
        mp_drawing.draw_landmarks(image, results.pose_landmarks, mp_pose.POSE_CONNECTIONS,
                                mp_drawing.DrawingSpec(color=(245,66,230), thickness=2, circle_radius=2), 
                                mp_drawing.DrawingSpec(color=(color), thickness=2, circle_radius=5))
        

        
      
        
        # Frames 
        cTime = time.time()
        fps = 1 / (cTime - pTime)
        pTime = cTime
        cv2.putText(image, str(int(fps)), (0, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 1, cv2.LINE_AA)   
                             
        
        cv2.imshow('Media player', image)

        if cv2.waitKey(10) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()